<?php

require_once 'conf.php';

$request = file_get_contents('php://input');
$obj = json_decode($request, TRUE);

$type = $obj['event_type'];

if($type == 'engagement.start') {
	$engagement_id = $obj['engagement']['id'];
	$engagement_request_id = $obj['engagement']['engagement_request_id'];
	$sql = 'UPDATE engagement SET 
	 			status = 2,
	 			engagement_id = '. escape($engagement_id) .'
	 		WHERE engagement_request_id = '. escape($engagement_request_id);
 	pg_query($db, $sql);	
 	sendInitialMessageToSaleMove($engagement_id);
}

?>